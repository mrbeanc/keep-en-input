#ifndef WIDGET_H
#define WIDGET_H

#include "inputmethod.h"
#include <QListWidget>
#include <QSystemTrayIcon>
#include <QWidget>
#include <windows.h>
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void saveList(void);
    void readList(void);
    QListWidgetItem* addItem(const QString& text = "", bool edit = false);

private:
    Ui::Widget* ui;

    const QString filePath = QApplication::applicationDirPath() + "/saveList.txt";
    InputMethod inputMethod;
    QSystemTrayIcon* sysTray = nullptr;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent* event) override;
};
#endif // WIDGET_H
