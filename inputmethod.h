#ifndef INPUTMETHOD_H
#define INPUTMETHOD_H

#include <QApplication>
#include <QList>
#include <QString>
#include <QStringList>
#include <windows.h>
class InputMethod
{
public:
    InputMethod();
    void setEnMode(HWND hwnd);
    void checkAndSetEn(void);
    void setList(const QStringList& strList);

private:
    HKL hkl;
    HWND lastHwnd = NULL;
    QStringList list;

private:
    bool isWindowTitleInList(const QString& title);
    bool isProcessNameInList(const QString& name);
    DWORD getForeWindowPID(HWND hwnd = NULL);
    QString getProcessNameByPID(DWORD PID);
    QString getWindowTitle(HWND hwnd = NULL);
};

#endif // INPUTMETHOD_H
