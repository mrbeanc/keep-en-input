#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QFile>
#include <QProcess>
#include <QStyle>
#include <QTime>
#include <QTimer>
#include <QtWin>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    QtWin::taskbarDeleteTab(this); //删除任务栏图标
    setWindowTitle("[KeepEnInput] By MrBeanC");

    sysTray = new QSystemTrayIcon(this);
    sysTray->setIcon(QIcon(":/EN.ico"));
    sysTray->setToolTip("KeepEnInput");
    connect(sysTray, &QSystemTrayIcon::activated, [=](QSystemTrayIcon::ActivationReason reason) {
        if (reason == QSystemTrayIcon::Trigger) {
            showMinimized();
            showNormal();
            activateWindow();
        }
    });
    sysTray->show();

    QTimer* timer = new QTimer(this);
    timer->callOnTimeout([=]() {
        QStringList list;
        int rows = ui->listWidget->count();
        for (int i = 0; i < rows; i++)
            list << ui->listWidget->item(i)->text();
        inputMethod.setList(list);
        inputMethod.checkAndSetEn();
    });
    timer->start(1000);

    connect(ui->btn_add, &QPushButton::clicked, [=]() {
        addItem("", true);
    });
    connect(ui->btn_del, &QPushButton::clicked, [=]() {
        int row = ui->listWidget->currentRow(); //获取当前鼠标所选行
        delete ui->listWidget->takeItem(row); //删除该行
    });
    connect(ui->listWidget, &QListWidget::itemDoubleClicked, ui->listWidget, &QListWidget::editItem); //editItem会开启编辑(转移焦点会自动关闭)
    ui->listWidget->installEventFilter(this); //closePersistentEditor会保持edit after editItem转移焦点后

    QTimer::singleShot(0, [=]() { readList(); });

    sysTray->showMessage("This is Title", "KeepEnInput Has Started\n为您的EN输入保驾护航");
    //感觉会被喷啊 毕竟从前先辈想方设法将中文带入PC 而此软件费尽心思维持EN
}

Widget::~Widget()
{
    delete ui;
}

void Widget::saveList()
{
    QFile file(filePath);
    if (file.open(QFile::Text | QIODevice::WriteOnly)) {
        QTextStream text(&file);
        text.setCodec("UTF-8");
        int rows = ui->listWidget->count();
        for (int i = 0; i < rows; i++)
            text << ui->listWidget->item(i)->text() << "\n";
        file.close();
    }
}

void Widget::readList()
{
    QFile file(filePath);
    if (file.open(QFile::Text | QFile::ReadOnly)) {
        QTextStream text(&file);
        text.setCodec("UTF-8");
        while (!text.atEnd()) {
            QString line = text.readLine();
            //ui->listWidget->addItem(line);
            addItem(line);
        }
        file.close();
    }
}

QListWidgetItem* Widget::addItem(const QString& text, bool edit)
{
    QListWidgetItem* item = new QListWidgetItem(text, ui->listWidget);
    item->setFlags(item->flags() | Qt::ItemIsEditable); //否则不能editItem
    ui->listWidget->addItem(item);
    if (edit) {
        ui->listWidget->editItem(item);
        //ui->listWidget->setCurrentRow(ui->listWidget->row(item)); //currentRow与selected不同 焯（因为select可以多选）
        ui->listWidget->setCurrentItem(item); //与↑差不多
    }
    return item;
}

void Widget::closeEvent(QCloseEvent*)
{
    hide();
    saveList();
}
